-- Abstract Semantic Representation (ASR) definition

-- The aim of ASR is to represent all semantics in a non-redundant way, and that
-- has all the semantic information available locally, so that the backend can
-- do a single pass over ASR and have all the information at hand to generate
-- code.
--
-- ASR is always semantically valid Fortran code. It is as far from the original
-- Fortran language code as possible (i.e. everthing is explicitly figured out,
-- all semantic information gathered and readily available locally from each ASR
-- node), while ensuring no semantic information was lost (no lowering was
-- done), so one can still generate Fortran code from ASR that will be logically
-- equivalent to the original code.
--
-- ASR can be used to do Fortran level transformations (such as optimizations).

-- ASDL's builtin types are:
--   * identifier
--   * int (signed integers of infinite precision)
--   * string
-- We extend these by:
--   * bool (.true. / .false.)
--   * symbol_table (scoped Symbol Table implementation)
--   * node (any ASR node)
--
-- Note: `symbol_table` contains `identifier` -> `symbol` mappings.

module ASR {

unit
    = TranslationUnit(symbol_table global_scope, node* items)

-- # Documentation for the symbol type

-- Each symbol has either `symtab` (local symbol table) or `parent_symtab`
-- (where this symbol is stored). One can get to parent_symtab via symtab, so
-- only one is present.

-- Each symbol has a `name` for easy lookup of the name of the symbol when only
-- having a pointer to it.

-- abi=Source means the symbol's implementation is included (full ASR),
-- otherwise it is external (interface ASR, such as procedure interface).

-- SubroutineCall/FunctionCall store the actual final resolved subroutine or
-- function (`name` member). They also store the original symbol
-- (`original_name`), which can be one of: null, GenericProcedure or
-- ExternalSymbol.

-- When a module is compiled, it is parsed into full ASR, an object file is
-- produced, the full ASR (abi=Source, "body" is non-empty) is transformed into
-- interface ASR (abi=LFortran, "body" is empty). Both interface and full ASR
-- is saved into the mod file.

-- When a module is used, it is first looked up in the symbol table (as either
-- full or interface ASR) and used if it is present. Otherwise a mod file is
-- found on the disk, loaded (as either full or interface ASR for LFortran's
-- mod file, depending on LFortran's compiler options; or for GFortran's mod
-- file the corresponding interface ASR is constructed with abi=GFortran) and
-- used. After the ASR is loaded, the symbols that are used are represented as
-- ExternalSymbols in the current scope of the symbol table.

-- ExternalSymbol represents symbols that cannot be looked up in the current
-- scoped symbol table. As an example, if a variable is defined in a module,
-- but used in a nested subroutine, that is not an external symbol
-- because it can be resolved in the current symbol table (nested subroutine)
-- by following the parents. However if a symbol is used from a different
-- module, then it is an external symbol, because usual symbol resolution by
-- going to the parents will not find the definition.

-- REPL: each cell is parsed into full ASR, compiled + executed, the full ASR
-- is transformed into interface ASR (abi=LFortran) and kept in the symbol
-- table. A new cell starts with an empty symbol table, whose parent symbol
-- table is the previous cell. That allows function / declaration shadowing.


symbol
    = Program(symbol_table symtab, identifier name, identifier* dependencies,
        stmt* body)
    | Module(symbol_table symtab, identifier name, identifier* dependencies,
        bool loaded_from_mod)
    | Subroutine(symbol_table symtab, identifier name, expr* args, stmt* body,
        abi abi, access access, deftype deftype)
    | Function(symbol_table symtab, identifier name, expr* args, stmt* body,
        expr return_var, abi abi, access access, deftype deftype)
    | GenericProcedure(symbol_table parent_symtab, identifier name,
        symbol* procs, access access)
    | ExternalSymbol(symbol_table parent_symtab, identifier name,
        symbol external, identifier module_name, identifier original_name,
        access access)
    | DerivedType(symbol_table symtab, identifier name, abi abi, access access)
    | Variable(symbol_table parent_symtab, identifier name, intent intent,
        expr? value, storage_type storage, ttype type, abi abi, access access)

storage_type = Default | Save | Parameter
access = Public | Private
intent = Local | In | Out | InOut | ReturnVar | Unspecified
deftype = Implementation | Interface

-- # Documentation for the ABI type

-- External Yes: the symbol's implementation is not part of ASR, the
-- symbol is just an interface (e.g., subroutine/function interface, or variable
-- marked as external, not allocated by this ASR).

-- External No:  the symbol's implementation is part of ASR (e.g.,
-- subroutine/function body is included, variables must be allocated).

-- abi=Source: The symbol's implementation is included in ASR, the backend is
-- free to use any ABI it wants (it might also decide to inline or eliminate
-- the code in optimizations).

-- abi=LFortranModule/GFortranModule/BindC: the symbol's implementation is
-- stored as machine code in some object file that needs to be linked in. It
-- uses the specified ABI (one of LFortran module, GFortran module or C ABI).
-- An interface that uses `iso_c_binding` and `bind(c)` is represented using
-- abi=BindC.

-- abi=Interactive: the symbol's implementation has been provided by the
-- previous REPL execution (e.g., if LLVM backend is used for the interactive
-- mode, the previous execution generated machine code for this symbol's
-- implementation that was loaded into memory). Note: this option might be
-- converted/eliminated to just use LFortran ABI in the future.

-- abi=Intrinsic: the symbol's implementation is implicitly provided by the
-- language itself as an intrinsic function. That means the backend is free to
-- implement it in any way it wants. The function does not have a body, it is
-- just an interface.

abi                   -- External     ABI
    = Source          --   No         Unspecified
    | LFortranModule  --   Yes        LFortran
    | GFortranModule  --   Yes        GFortran
    | BindC           --   Yes        C
    | Interactive     --   Yes        Unspecified
    | Intrinsic       --   Yes        Unspecified


stmt
    = Allocate()
    | Assignment(expr target, expr value)
    | Associate(expr target, expr value)
    | Cycle()
    | Deallocate()
    | DoConcurrentLoop(do_loop_head head, stmt* body)
    | DoLoop(do_loop_head head, stmt* body)
    | ErrorStop(expr? code)
    | Exit()
    | If(expr test, stmt* body, stmt* orelse)
    | Print(string? fmt, expr* values)
    | Open(int label, expr? newunit, expr? filename, expr? status)
    | Close(int label, expr? unit, expr? iostat, expr? iomsg, expr? err, expr? status)
    | Read(int label, expr? unit, expr? fmt, expr? iomsg, expr? iostat, expr? id, expr* values)
    | Write(int label, expr? unit, expr? fmt, expr? iomsg, expr? iostat, expr? id, expr* values)
    | Return()
    | Select(expr test, case_stmt* body, stmt* default)
    | Stop(expr? code)
    | SubroutineCall(symbol name, symbol? original_name, expr* args)
    | Where(expr test, stmt* body, stmt* orelse)
    | WhileLoop(expr test, stmt* body)


expr
    = BoolOp(expr left, boolop op, expr right, ttype type)
    | BinOp(expr left, binop op, expr right, ttype type)
    | StrOp(expr left, strop op, expr right, ttype type)
    | UnaryOp(unaryop op, expr operand, ttype type)
    | Compare(expr left, cmpop op, expr right, ttype type)
    | FunctionCall(symbol name, symbol? original_name, expr* args,
            keyword* keywords, ttype type)
    | ArrayInitializer(expr* args, ttype type)
    | ImpliedDoLoop(expr* values, expr var, expr start, expr end,
                    expr? increment, ttype type)
    | ConstantInteger(int n, ttype type)
    | ConstantReal(string r, ttype type)
    | ConstantComplex(expr re, expr im, ttype type)
    | ConstantLogical(bool value, ttype type)
    | Str(string s, ttype type)
    | Var(symbol v)
    | ArrayRef(symbol v, array_index* args, ttype type)
    | DerivedRef(expr v, symbol m, ttype type)
    | ImplicitCast(expr arg, cast_kind kind, ttype type)
    | ExplicitCast(expr arg, cast_kind kind, ttype type)

ttype
    = Integer(int kind, dimension* dims)
    | Real(int kind, dimension* dims)
    | Complex(int kind, dimension* dims)
    | Character(int kind, dimension* dims)
    | Logical(int kind, dimension* dims)
    | Derived(symbol derived_type, dimension* dims)
    | IntegerPointer(int kind, dimension* dims)
    | RealPointer(int kind, dimension* dims)
    | ComplexPointer(int kind, dimension* dims)
    | CharacterPointer(int kind, dimension* dims)
    | LogicalPointer(int kind, dimension* dims)
    | DerivedPointer(symbol derived_type, dimension* dims)

boolop = And | Or | NEqv | Eqv

binop = Add | Sub | Mul | Div | Pow

unaryop = Invert | Not | UAdd | USub

strop = Concat

cmpop = Eq | NotEq | Lt | LtE | Gt | GtE

cast_kind
    = RealToInteger
    | IntegerToReal
    | RealToReal
    | IntegerToInteger
    | RealToComplex
    | IntegerToComplex
    | IntegerToLogical
    | ComplexToComplex

dimension = (expr? start, expr? end)

attribute = Attribute(identifier name, attribute_arg *args)

attribute_arg = (identifier arg)

arg = (identifier arg)

keyword = (identifier? arg, expr value)

tbind = Bind(string lang, string name)

array_index = (expr? left, expr? right, expr? step)

do_loop_head = (expr v, expr start, expr end, expr? increment)

case_stmt = CaseStmt(expr* test, stmt* body) | CaseStmt_Range(expr? start, expr? end, stmt* body)

}
